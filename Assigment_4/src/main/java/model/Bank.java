package model;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Vector;

import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;


public class Bank implements BankProc, Serializable{
	
	private static final long serialVersionUID = 1L;
	private HashMap<Person, Set<Account>> hashmap;
	private static final String BANK_FILE = "F:\\PT2017_30423_PeterCatalinBogdan_Assigment_4\\Assigment_4\\persons.ser";


	public Bank() {
		hashmap = new HashMap<Person, Set<Account>>();
		
	}
	/** This method adds new persons in the bank
	 * @param p is the person we want to add
	 * @pre p !=null
	 * @pre !hashmap.keySet().contains(p)
	 * @post hashmap.size() == size
	 * @invariant isWellFormed()
	 */
	public void addPerson(Person p) {
		assert(p != null);
		assert isWellFormed();
		assert(!hashmap.keySet().contains(p));
		
		int size = hashmap.size() + 1;
		hashmap.put(p, new HashSet<Account>());
		
		assert isWellFormed();
		assert( hashmap.size() == size);
	}
	/**
	 * This method removes a person from the bank
	 * @param p is the person we want to remove
	 * @pre p != null
	 * @pre hashmap.keyset().contains(p)
	 * @post hashmap.size() == size
	 * @invariant isWellFormed()
	 * 
	 */
	public void removePerson(Person p) {
		assert(p != null);
		assert isWellFormed();
		assert(hashmap.keySet().contains(p));
		
		int size = hashmap.size() - 1;
		hashmap.remove(p);
		
		assert isWellFormed();
		assert(hashmap.size() == size);
	}
	/**
	 * This method adds a new account to the bank
	 * @param p is the person for which we add the account
	 * @param a is the account we want to add
	 * @pre p != null
	 * @pre a != null
	 * @pre !hashmap.get(p).contains(a)
	 * @post hashmap.get(p).size() == size
	 * @invariant isWellFormed();
	 */
	public void addAccount(Person p, Account a) {
		assert(p != null);
		assert(a != null);
		assert(!hashmap.get(p).contains(a));
		assert isWellFormed();
		
		a.addObserver(p);
		Set<Account> temporaryAccount = hashmap.get(p);
	    int	size = temporaryAccount.size() + 1;
		temporaryAccount.add(a);
		hashmap.replace(p, temporaryAccount);
		
		assert isWellFormed();
		assert(hashmap.get(p).size() == size);
		
	}
	/**
	 * This method removes an account from the bank
	 * @param a is the account we want to remove
	 * @pre a != null
	 * @pre hashmap.get(a.getPerson()).contains(a)
	 * @post size == hashmap.get(p).size()
	 * @invariant isWellFormed()
	 */
	public void removeAccount(Account a) {
		assert(a != null);
		assert(hashmap.get(a.getPerson()).contains(a));
		assert isWellFormed();
		
		Person p = a.getPerson();
		Set<Account> temporaryAccount = hashmap.get(p);
		int size = temporaryAccount.size() - 1;
		temporaryAccount.remove(a);
		hashmap.replace(p, temporaryAccount);
		
		assert isWellFormed();
		assert(size == hashmap.get(p).size());
	}
	/**
	 * This method reads an account from the bank using the personId and accountID
	 * @param personId is the persons id
	 * @param accountId is the accounts id
	 * @pre personId > -1
	 * @pre accountId > -1
	 * @invariant isWellFormed();
	 */
	public Account readAccount(int personId, int accountId) {
		assert(personId > -1);
		assert(accountId > -1);
		assert isWellFormed();
		
		Set<Person> temporaryPerson = hashmap.keySet();
		for (Person p : temporaryPerson) {
			if (p.getId() == personId) {
				Set<Account> temporaryAccount = hashmap.get(p);
				for (Account a : temporaryAccount) {
					if (a.getId() == accountId)
						return a;
				}
			}
		}
		assert isWellFormed();
	return null;	
	}
	private boolean isWellFormed() {
		for (Person p : hashmap.keySet()) {
			if (p == null)
				return false;
			if (!(hashmap.get(p) instanceof Set<?>)) {
				return false;
			}
		}
		
		return true;
		
	}

	public JTable createPersonsJTable() {
		JTable table;
		PersonsTableModel model;
		List<Person> rawData = new ArrayList<Person>();
		for (Person p : hashmap.keySet()){
			rawData.add(p);
		}
		model = new PersonsTableModel(rawData);
		table = new JTable(model);
		return table;
	}
	public JTable createAccountJTable(Person p) {
		JTable table;
		AccountsTableModel model;
		List<Account> rawData = new ArrayList<Account>();
		for (Account a : hashmap.get(p)){
			rawData.add(a);
		}
		model = new AccountsTableModel(rawData);
		table = new JTable(model);
		return table;
	}

	public void saveToFile() {
		try {
			FileOutputStream fout= new FileOutputStream(BANK_FILE);
			ObjectOutputStream oos = new ObjectOutputStream(fout);
			oos.writeObject(this);
			oos.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public Bank loadFromFile() {
		Bank bank = new Bank();
		try {
			FileInputStream fin = new FileInputStream(BANK_FILE);
			ObjectInputStream ois = new ObjectInputStream(fin);
			bank = (Bank) ois.readObject();
			for (Person p : bank.hashmap.keySet()){
				if (p.getId() > Person.getIdCount())
					Person.setIdCount(p.getId() + 1);
			}
			for (Set<Account> s : bank.getHashMap().values()) {
				for (Account a : s){
					a.addObserver(a.getPerson());
					if (a.getId() > Account.getIdCount())
						Account.setIdCount(a.getId() + 1);
				}
			}
			ois.close();
			fin.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return bank;

	}
	public HashMap<Person,Set<Account>> getHashMap() {
		return this.hashmap;
	}
	public void print() {
		for (Person p : hashmap.keySet()) {
			System.out.println(p);
			for (Account a : hashmap.get(p)) {
				System.out.println(a);
			}
		}
	}
	
	
	

}
