package PT2017.demo.Assigment_4;

import model.Account;
import model.Bank;
import model.Person;
import model.SavingAccount;
import model.SpendingAccount;
import presentation.Controller;
import presentation.View;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
    	View view = new View();
    	Controller controller = new Controller(view);
    }
}
