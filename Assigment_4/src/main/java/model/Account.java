package model;

import java.io.Serializable;
import java.util.Observable;

public class Account extends Observable implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int id;
	private Person person;
	private String type;
	private float money;
	private static int idCount = 0;
	
	public Account(String type) {
		this.id = idCount++;
		this.type = type;
		this.person = new Person();
		this.money = 0;
	}
	public Account(Person person, float money, String type) {
		this.id = idCount++;
		this.type = type;
		this.person = person;
		this.money = money;
	}

	public int getId() {
		return this.id;
	}
	public void setId(int id) {
	this.id = id;
	}
	
	public String getType() {
		return this.type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
	public Person getPerson() {
		return this.person;
	}
	public void setPerson(Person person) {
		this.person = person;
	}
	
	public static int getIdCount() {
		return idCount;
	}
	public static void setIdCount(int id) {
		idCount = id;
	}
	public float getMoney() {
		return this.money;
	}
	public void setMoney(float money) {
		this.money = money;
	}
	
	public String toString() {
		String out = String.valueOf(id) + " " + person.toString() + " " + String.valueOf(money);
		return out;
	}

}
