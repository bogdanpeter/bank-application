package model;

public class SpendingAccount extends Account{
	
	public SpendingAccount() {
		super("spending");
	}
	public SpendingAccount(Person person, float money) {
		super(person, money, "spending");
	}
	public void addMoney(float amount) {
		float aux = super.getMoney() + amount;
		super.setMoney(aux);
		setChanged();
		notifyObservers(super.getPerson());
	}
	public float withdrawMoney(float amount) throws Exception {
		float aux = super.getMoney();
		if (amount > aux)
			throw new Exception("Not enough money in the account!");
		else {
			aux = aux - amount;
			super.setMoney(aux);
			setChanged();
			notifyObservers();
			return amount;
		}
	}
}
