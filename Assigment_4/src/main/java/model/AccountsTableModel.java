package model;

import java.util.List;

public class AccountsTableModel extends GenericTableModel<Account> {
	private static final long serialVersionUID = 1L;
	private static final String[] columnNames = { "id", "type", "money"};
	
	public AccountsTableModel() {
		super(columnNames);
	}
	public AccountsTableModel(List<Account> accounts) {
		super(accounts, columnNames);
	}
}
