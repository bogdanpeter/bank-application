package presentation;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.DefaultCellEditor;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.event.TableModelListener;

import model.Account;
import model.AccountsTableModel;
import model.Bank;
import model.Person;
import model.PersonsTableModel;
import model.SavingAccount;
import model.SpendingAccount;

public class View  extends JFrame{
	private static final long serialVersionUID = 1L;
	
	
	JFrame personsFrame;
	JTable personsTable;
	JTable accountsTable;
	
	public View() {
		personsFrame = new JFrame("Persons");
	}
	
	public JFrame createPersonsFrame(Bank bank, ActionListener create, ActionListener open, ActionListener delete, MouseListener edit) {
		JButton createBtn = new JButton("Create Person");
		JButton openBtn = new JButton("Open Accounts");
		JButton editBtn = new JButton("Edit Person");
		JButton deleteBtn = new JButton("Delete Person");
		personsFrame.setLayout(null);
		personsTable = bank.createPersonsJTable();
		createBtn.addActionListener(create);
		personsTable.addMouseListener(edit);
				
		/*editBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				editPersonFrame();
			}
		});*/
		openBtn.addActionListener(open);
		deleteBtn.addActionListener(delete);
		JScrollPane  pane = new JScrollPane(personsTable);

		pane.setBounds(35, 20, 600, 600);
		personsFrame.add(pane);
		createBtn.setBounds(20,650, 150, 30);
		personsFrame.add(createBtn);
		openBtn.setBounds(180, 650, 150, 30);
		personsFrame.add(openBtn);
		deleteBtn.setBounds(340, 650, 150, 30);
		personsFrame.add(deleteBtn);
		editBtn.setBounds(500, 650, 150, 30);
//		personsFrame.add(editBtn);
		
		personsFrame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		personsFrame.setLocation(0, 50);
		personsFrame.setBounds(0, 0, 700, 750);
		personsFrame.setVisible(true);
		return personsFrame;
	}
	public JFrame createAccountsFrame(Bank bank, Person person, ActionListener deposit, ActionListener withdraw){
		JFrame accountsFrame;
		final Person p = person;
		final Bank b = bank;
		JButton createBtn = new JButton("Create Account");
		JButton deleteBtn = new JButton("Delete Account");
		JButton depositBtn = new JButton("Deposit");
		JButton withdrawBtn = new JButton("Withdraw");
		accountsFrame = new JFrame("Accounts");
		accountsFrame.setLayout(null);
		accountsFrame.setBounds(0, 0, 700, 750);
		this.accountsTable = bank.createAccountJTable(person);
		createBtn.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				createAccountFrame(b, p);
			}
				
		});
		createBtn.setBounds(20,	650, 150, 30);
		depositBtn.addActionListener(deposit);
		depositBtn.setBounds(180, 650, 150, 30);
		withdrawBtn.addActionListener(withdraw);
		withdrawBtn.setBounds(340, 650, 150, 30);
		deleteBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				try{
					if (accountsTable.getSelectedRow() < 0) {
						showError("Select an account first!");
					}else {
						AccountsTableModel model = (AccountsTableModel)accountsTable.getModel();
						Account a = (Account) model.getObject(accountsTable.getSelectedRow());
						b.removeAccount(a);
						model.removeObject(accountsTable.getSelectedRow());
						model.fireTableDataChanged();
					}
				} catch (Exception e){
					showError(e.getMessage());
				}
			}
		
		});
		deleteBtn.setBounds(500, 650, 150, 30);
		JScrollPane pane = new JScrollPane(accountsTable);
		pane.setBounds(35, 20, 600, 600);

		accountsFrame.add(pane);
		accountsFrame.add(createBtn);
		accountsFrame.add(depositBtn);
		accountsFrame.add(withdrawBtn);
		accountsFrame.add(deleteBtn);
		accountsFrame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		accountsFrame.setLocation(200, 0);
		accountsFrame.setVisible(true);
		return accountsFrame;
	}
	public void createAccountFrame(Bank bank, Person person){
		final Person p = person;
		final Bank b = bank;
		final JFrame frame = new JFrame("Create Account");
		JLabel type = new JLabel("Type: ");
		type.setBounds(20, 100, 80, 30);
		final String[] boxContent ={ "Savings", "Spending"};
		final JComboBox<String> box = new JComboBox<String>(boxContent);
		box.setEditable(false);
		box.setBounds(120, 100, 150, 30);
		JLabel moneyL = new JLabel("Money: ");
		moneyL.setBounds(20, 50, 80, 30);
		final JTextField moneyTf = new JTextField();
		moneyTf.setBounds(120, 50, 150, 30);
		JLabel timeL = new JLabel("Time: ");
		timeL.setBounds(20, 150, 80, 30);
		final JTextField timeTf = new JTextField();
		timeTf.setText("0");
		timeTf.setBounds(120, 150, 150, 30);;
		JButton done = new JButton("Done");
		done.setBounds(120, 200, 80, 30);
		done.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent event) {
				try {
					Account account;
					AccountsTableModel model = (AccountsTableModel) accountsTable.getModel();
					Float money;
					if (box.getSelectedItem().equals(boxContent[0])){
						int time = Integer.valueOf(timeTf.getText());
						if (time < 2) {
							showError("The minimum time for opening a savings account is 2");
						} else {
							money = Float.valueOf(moneyTf.getText());
							if ( money < 200) {
								showError("The minimum amount of money for opening a savings account is 200");
							} else {
								account = new SavingAccount(p, money, time);
								b.addAccount(p, account);
								model.addObject(account);
								frame.dispose();
							}
						}
					} else {
						money = Float.valueOf(moneyTf.getText());
						account = new SpendingAccount(p, money);
						b.addAccount(p, account);
						model.addObject(account);
						frame.dispose();
					}
					
				} catch (Exception e) {
					showError(e.getMessage());
				}
			}
			
		});
		
		frame.add(type);
		frame.add(box);
		frame.add(moneyL);
		frame.add(moneyTf);
		frame.add(timeL);
		frame.add(timeTf);
		frame.add(done);
		frame.setLayout(null);
		frame.setBounds(0, 0, 350, 350);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.setVisible(true);
		
		
	}
	public void editPersonFrame() {
		final JFrame frame = new JFrame("Edit Person");
		final PersonsTableModel model = (PersonsTableModel) personsTable.getModel();
		final Person p = (Person) model.getObject(personsTable.getSelectedRow());
		frame.setLayout(null);
		frame.setBounds(0, 0, 300, 150);
		JLabel nameL = new JLabel("Name:");
		nameL.setBounds(10, 20, 100, 30);
		final JTextField nameTf = new JTextField();
		nameTf.setBounds(90, 20, 150, 30);
		nameTf.setText(p.getName());
		JButton doneBtn = new JButton("Done");
		doneBtn.setBounds(80, 60, 150, 30);
		frame.add(nameL);
		frame.add(nameTf);
		frame.add(doneBtn);
		doneBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				try {
					p.setName(nameTf.getText());
					model.fireTableDataChanged();
					frame.dispose();
				} catch (Exception e) {
					showError(e.getMessage());
				}
			}
			
		});
		frame.setLocation(250, 500);
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
	}
	public void addPersonFrame(Bank bank) {
		final Bank b = bank;
		final JFrame frame = new JFrame();
		frame.setLayout(null);
		frame.setBounds(0, 0, 400, 200);
		JLabel nameL = new JLabel("Name:");
		nameL.setBounds(10, 60, 100, 30);
		final JTextField nameTf = new JTextField();
		nameTf.setBounds(120, 60, 250, 30);
		JButton doneBtn = new JButton("Done");
		doneBtn.setBounds(50, 95, 150, 30);
		frame.add(nameL);
		frame.add(nameTf);
		frame.add(doneBtn);
		doneBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				try {
					Person p = new Person(nameTf.getText());
					b.addPerson(p);
					PersonsTableModel model = (PersonsTableModel) personsTable.getModel();
					model.addObject(p);
					frame.dispose();
				} catch (Exception e) {
					showError(e.getMessage());
				}
			}
			
		});
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
	}
	public void depositFrame(SpendingAccount a) {
		final SpendingAccount account = a;
		final AccountsTableModel model = (AccountsTableModel) accountsTable.getModel();
		final JFrame frame = new JFrame("Deposit");
		frame.setLayout(null);
		frame.setBounds(0, 0, 200, 150);
		final JTextField money = new JTextField();
		money.setBounds(50, 10, 90, 30);
		frame.add(money);
		JButton done = new JButton("done");
		done.setBounds(50, 45, 90, 30);
		frame.add(done);
		done.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				try {
					float m = Float.valueOf(money.getText());
					account.addMoney(m);
					model.fireTableDataChanged();
					frame.dispose();
				} catch (Exception e) {
					showError(e.getMessage());
				}
			}
		});
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.setLocation(500,500);
		frame.setVisible(true);
	}
	public void withdrawFrame(SpendingAccount a) {
		final SpendingAccount account = a;
		final AccountsTableModel model = (AccountsTableModel) accountsTable.getModel();
		final JFrame frame = new JFrame("Withdraw");
		frame.setLayout(null);
		frame.setBounds(0, 0, 200, 150);
		final JTextField money = new JTextField();
		money.setBounds(50, 10, 90, 30);
		frame.add(money);
		JButton done = new JButton("done");
		done.setBounds(50, 45, 90, 30);
		frame.add(done);
		done.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				try {
					float amount = Float.valueOf(money.getText());
					account.withdrawMoney(amount);
					model.fireTableDataChanged();
					frame.dispose();
				} catch (Exception e) {
					showError(e.getMessage());
				}
			}
		});
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.setLocation(500,500);
		frame.setVisible(true);
	}
	public JTable getAccountsTable(){
		return this.accountsTable;
	}
	public JTable getPersonsTable() {
		return this.personsTable;
	}
	void showError(String errMessage) {
		JOptionPane.showMessageDialog(new JPanel(), errMessage);
		}
}
