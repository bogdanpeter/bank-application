package model;

import java.util.List;

public class PersonsTableModel extends GenericTableModel<Person> {
	private static final long serialVersionUID = 1L;
	private static final String[] columnNames = { "id", "name"};
	
	public PersonsTableModel() {
		super(columnNames);
	}
	public PersonsTableModel(List<Person> persons) {
		super(persons, columnNames);
	}
}
