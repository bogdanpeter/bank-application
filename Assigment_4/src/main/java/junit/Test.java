package junit;

import junit.framework.TestCase;
import model.Account;
import model.Bank;
import model.Person;
import model.SavingAccount;
import model.SpendingAccount;

public class Test extends TestCase {
	Bank bank = new Bank();
	Person p1 = new Person("p1");
	Person p2 = new Person("p2");
	Account a1;
	Account a2;
	Account a3;
	public void testCreateSavingAccount() {
		a1 = new SavingAccount(p1, 100, 3);
		bank.addPerson(p1);
		bank.addAccount(p1, a1);
		assertTrue(bank.getHashMap().get(p1).contains(a1));
	}
	public void testCreateSpendingAccount() {
		a2 = new SpendingAccount(p1, 150);
		bank.addPerson(p2);
		bank.addAccount(p2, a2);
		assertTrue(bank.getHashMap().get(p2).contains(a2));
	}
	public void testDeposit() {
		float money = 150;
		a1 = new SpendingAccount(p1, 50);
		bank.addPerson(p1);
		bank.addAccount(p1, a1);
		((SpendingAccount) a1).addMoney(100);
		Account a = bank.readAccount(p1.getId(), a1.getId());
		assertEquals(money, a.getMoney());
	}
	public void testWithdraw() {
		a2 = new SpendingAccount(p2, 300);
		float money = 150;
		bank.addPerson(p2);
		bank.addAccount(p2, a2);
		try {
			((SpendingAccount) a2).withdrawMoney(150);
		} catch (Exception e) {
			e.printStackTrace();
		}
		Account a = bank.readAccount(p2.getId(), a2.getId());
		assertEquals(money, a.getMoney());
	}
	public void testRemoveAccount() {
		bank.addPerson(p1);
		a1 = new SpendingAccount(p1, 150);
		a2 = new SpendingAccount(p1, 200);
		bank.addAccount(p1, a1);
		bank.addAccount(p1, a2);
		bank.removeAccount(a1);
		bank.removeAccount(a2);
		System.out.println(bank.getHashMap().get(p1).size());
		assertTrue(bank.getHashMap().get(p1).size() == 0);
	}
}
