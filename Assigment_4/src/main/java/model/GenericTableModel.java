package model;

import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;

public class GenericTableModel<T> extends AbstractTableModel {

	private static final long serialVersionUID = 1L;
	private  String[] columnNames;
	private List<T> objects;
	private final Class<T> type;
	@SuppressWarnings("unchecked")
	public GenericTableModel(String[] columnNames) {
		super();
		this.type = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
		objects = new ArrayList<T>();
		this.columnNames = columnNames;
		
	}
	@SuppressWarnings("unchecked")
	public GenericTableModel(List<T> objects, String[] columnNames) {
		super();
		this.objects = objects;
		this.type = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
//		objects = new ArrayList<T>();
		this.columnNames = columnNames;
	}
	public int getColumnCount() {
		return columnNames.length;
	}
	@Override
	public String getColumnName(int column) {
		return columnNames[column];
	}
	public int getRowCount() {
		return objects.size();
	}
	@Override
	public Class<?> getColumnClass(int column) {
		for (Field field : type.getDeclaredFields()) {
			if(field.getName().equals(columnNames[column]))
				return field.getType();
		}
		return null;
	}
	
	public Object getValueAt(int row, int column) {
		try {
		T instance = getInstance(row);
		PropertyDescriptor propertyDescriptor = new PropertyDescriptor(columnNames[column], type);
		Method method = propertyDescriptor.getReadMethod();
		return method.invoke(instance);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	@Override
	public void setValueAt(Object value, int row, int column) {
		T instance = getInstance(row);
		try {
			PropertyDescriptor propertyDrescriptor = new PropertyDescriptor(columnNames[column], type);
			Method method = propertyDrescriptor.getWriteMethod();
			method.invoke(instance, value);
		} catch (Exception e) {
			e.printStackTrace();
		} 
		fireTableCellUpdated(row, column);
	}
	public Object getObject(int row) {
		return objects.get(row);
	}
	public void addObject(Object object) {
		insertObject(getRowCount(), object);
	}
	@SuppressWarnings("unchecked")
	public void insertObject(int row, Object object) {
		objects.add(row, (T) object);
		fireTableRowsInserted(row, row);
	}
	public void removeObject(int row) {
		objects.remove(row);
		fireTableDataChanged();
	}
	private T getInstance(int row) {
		return objects.get(row);
	}
	
	

}
