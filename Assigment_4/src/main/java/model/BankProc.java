package model;

public interface BankProc {
	/** This method adds new persons in the bank
	 * @param p is the person we want to add
	 * @pre p !=null
	 * @pre !hashmap.keySet().contains(p)
	 * @post hashmap.size() == size
	 * @invariant isWellFormed()
	 */
	public void addPerson(Person p);
	/**
	 * This method removes a person from the bank
	 * @param p is the person we want to remove
	 * @pre p != null
	 * @pre hashmap.keyset().contains(p)
	 * @post hashmap.size() == size
	 * @invariant isWellFormed()
	 * 
	 */
	public void removePerson(Person p);
	/**
	 * This method adds a new account to the bank
	 * @param p is the person for which we add the account
	 * @param a is the account we want to add
	 * @pre p != null
	 * @pre a != null
	 * @pre !hashmap.get(p).contains(a)
	 * @post hashmap.get(p).size() == size
	 * @invariant isWellFormed();
	 */
	public void addAccount(Person p, Account a);
	/**
	 * This method removes an account from the bank
	 * @param a is the account we want to remove
	 * @pre a != null
	 * @pre hashmap.get(a.getPerson()).contains(a)
	 * @post size == hashmap.get(p).size()
	 * @invariant isWellFormed()
	 */
	public void removeAccount(Account a);
	/**
	 * This method reads an account from the bank using the personId and accountID
	 * @param personId is the persons id
	 * @param accountId is the accounts id
	 * @pre personId > -1
	 * @pre accountId > -1
	 * @invariant isWellFormed();
	 */
	public Account readAccount(int personId, int accountId);
}
