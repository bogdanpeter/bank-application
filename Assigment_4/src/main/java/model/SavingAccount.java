package model;

import java.util.Date;

public class SavingAccount extends Account{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	Date date;
	float interest;
	int time;
	
	public SavingAccount() {
		super("savings");
		this.interest = 0;
		this.date = new Date();
	}
	public SavingAccount(Person person, float money, int time) {
		super(person, money, "savings");
		this.interest =  (1.5f * money) / 100;
		this.date = new Date();
		this.time = time;
	}
	public void addMoney(float money) throws Exception {
		throw new Exception("Cannot add money to a savings account");
	}
	public float withdrawMoney() throws Exception {
		int currentTime = (new Date().getMinutes() - date.getMinutes());
		if (super.getMoney() == 0)
			throw new Exception("The account is empty");
		if ( currentTime < this.time)
			throw new Exception("Can not withdraw money from this account at the current time");
		else {
			
			float aux = super.getMoney();
			super.setMoney(0);
			setChanged();
			notifyObservers();
			return (aux + (this.interest * currentTime));
		}
	}
}
