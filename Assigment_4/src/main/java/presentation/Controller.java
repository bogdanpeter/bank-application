package presentation;

import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;

import model.Account;
import model.AccountsTableModel;
import model.Bank;
import model.Person;
import model.PersonsTableModel;
import model.SavingAccount;
import model.SpendingAccount;

public class Controller {
	View view;
	private Bank bank;
	private JFrame personsFrame;
	private JFrame accountsFrame;
	private JTable personsTable;
	private JTable accountsTable;
	
	public Controller(View view) {
		this.view = view;
		this.bank =new Bank().loadFromFile();
		personsFrame = view.createPersonsFrame(this.bank,
				new CreatePersonListener(), 
				new OpenAccountsListener(),
				new DeletePersonListener(),
				new EditPersonListener());
		personsFrame.addWindowListener(new java.awt.event.WindowAdapter() {
		    @Override
		    public void windowClosing(java.awt.event.WindowEvent windowEvent) {
		        if (JOptionPane.showConfirmDialog(personsFrame, 
		            "Are you sure to close this window?", "Really Closing?", 
		            JOptionPane.YES_NO_OPTION,
		            JOptionPane.QUESTION_MESSAGE) == JOptionPane.YES_OPTION){
		        	bank.saveToFile();
		            System.exit(0);
		        }
		    }
		});
		this.personsTable = view.getPersonsTable();
	}
	class CreatePersonListener implements ActionListener {

		public void actionPerformed(ActionEvent event) {
				try{
					view.addPersonFrame(bank);
				}catch (Exception e) {
					view.showError(e.getMessage());
				}
		}

	}
	class OpenAccountsListener implements ActionListener {
		public void actionPerformed(ActionEvent event) {
			try {
				if (personsTable.getSelectedRow() < 0) {
					view.showError("Select a person first!");
				}else {
				PersonsTableModel model = (PersonsTableModel) personsTable.getModel();				
				Person p = (Person) model.getObject(personsTable.getSelectedRow());
				accountsFrame = view.createAccountsFrame(bank, p, new DepositMoneyListener(), new WithdrawMoneyListener());
				accountsTable = view.getAccountsTable();
				}
			} catch (Exception e) {
				e.printStackTrace();
				view.showError(e.getMessage());
			}
		}
	}
	class WithdrawMoneyListener implements ActionListener {
		public void actionPerformed(ActionEvent event) {
			try {
				if (accountsTable.getSelectedRow() < 0) {
					view.showError("Select an account first!");
				}else {
					AccountsTableModel model = (AccountsTableModel) accountsTable.getModel();
					if (model.getValueAt(accountsTable.getSelectedRow(), 1).equals("savings")){
						SavingAccount a = (SavingAccount) model.getObject(accountsTable.getSelectedRow());
						float money = a.withdrawMoney();
						view.showError("Withdraw succesful the sum of " + money + "! \n Deleting account.");
						bank.removeAccount(a);
						model.removeObject(accountsTable.getSelectedRow());
						model.fireTableDataChanged();
					} else {
						SpendingAccount a = (SpendingAccount) model.getObject(accountsTable.getSelectedRow());
						view.withdrawFrame(a);
					}
				}
			} catch (Exception e) {
				view.showError(e.getMessage());
			}
		}
	}
	class EditPersonListener implements MouseListener{

		public void mouseClicked(MouseEvent event) {
			if (event.getClickCount() == 2){
				view.editPersonFrame();
			}
		}

		public void mouseEntered(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}

		public void mouseExited(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}

		public void mousePressed(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}

		public void mouseReleased(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}
	};
	class OpenAccountssListener implements ActionListener {
		public void actionPerformed(ActionEvent event) {
			try {
				if (personsTable.getSelectedRow() < 0) {
					view.showError("Select a person first!");
				}else {
					PersonsTableModel model = (PersonsTableModel) personsTable.getModel();				
					Person p = (Person) model.getObject(personsTable.getSelectedRow());
					accountsFrame = view.createAccountsFrame(bank, p, new DepositMoneyListener(), null);
					accountsTable = view.getAccountsTable();
				}
			} catch (Exception e) {
				e.printStackTrace();
				view.showError(e.getMessage());
			}
		}
	}
	class DepositMoneyListener implements ActionListener {
		public void actionPerformed(ActionEvent event) {
			try {
				if (accountsTable.getSelectedRow() < 0) {
					view.showError("Select an account first!");
				}else {
					AccountsTableModel model = (AccountsTableModel) accountsTable.getModel();
					if (model.getValueAt(accountsTable.getSelectedRow(), 1).equals("savings")){
						view.showError("Cannot deposit in a savings account!");;
					} else {
						SpendingAccount a = (SpendingAccount) model.getObject(accountsTable.getSelectedRow());
						view.depositFrame(a);
						
					}
				}
			} catch (Exception e) {
				
			}
		}
	}
	class DeletePersonListener implements ActionListener {
		public void actionPerformed(ActionEvent event) {
			try {
				if (personsTable.getSelectedRow() < 0) {
					view.showError("Select a person first!");
				}else {
					PersonsTableModel model = (PersonsTableModel) personsTable.getModel();
					Person p = (Person) model.getObject(personsTable.getSelectedRow());
					bank.removePerson(p);
					model.removeObject(personsTable.getSelectedRow());
					model.fireTableDataChanged();
				}
			} catch (Exception e) {
				view.showError(e.getMessage());
			}
		}
	}
}
