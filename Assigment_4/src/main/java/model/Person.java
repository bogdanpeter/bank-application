package model;

import java.io.Serializable;
import java.util.Observable;
import java.util.Observer;

public class Person implements Serializable, Observer{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int id;
	private String name;
	private static int idCount = 0;
	
	public Person(){
		this.id = idCount++;
		this.name = "";
	}
	public Person(String name){
		this.id = idCount++;
		this.name = name;
	}
	public int getId() {
		return this.id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return this.name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public static void setIdCount(int id) {
		idCount = id;
	}
	public static int getIdCount() {
		return idCount;
	}
	
	public String toString() {
		String out = String.valueOf(id) + " " + name;
		return out;
	}
	public void update(Observable arg0, Object arg1) {
		Account a = (Account) arg0;
		if (a != null)
			System.out.println("The account for person: " + this.name + " was changed! New balance: " + a.getMoney() );
	}
	
}
